export const generateCreationDate = () => {
	const today = new Date();
	const d = String(today.getDate());
	const m = String(today.getMonth() + 1);
	const yyyy = today.getFullYear();
	return `${d}/${m}/${yyyy}`;
};
