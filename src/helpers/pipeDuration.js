export const pipeDuration = (duration) => {
	let hours = String(Math.floor(duration / 60));
	let minutes = String(duration - hours * 60);

	if (Number(hours) < 10) {
		hours = hours.padStart(2, 0);
	}
	if (Number(minutes) < 10) {
		minutes = minutes.padStart(2, 0);
	}
	return (
		<span>
			{hours}:{minutes}
		</span>
	);
};
