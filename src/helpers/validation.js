import { DURATION_REGEX } from '../constants';
export class ValidateInputs {
	validateName(name) {
		return name.length >= 2;
	}

	validateDescription(description) {
		return description.length >= 2;
	}

	validateDuration(duration) {
		return DURATION_REGEX.test(duration);
	}
}
