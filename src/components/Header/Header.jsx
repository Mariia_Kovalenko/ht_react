import Button from '../../common/Button/Button';
import Logo from './components/Logo/Logo';
import './Header.scss';

function Header() {
	return (
		<header className='header'>
			<Logo />
			<ul className='header__list'>
				<li className='header__item'>
					<div className='header__link'>John Doe</div>
				</li>
				<li className='header__item'>
					<Button
						buttonText={'Log Out'}
						onClick={() => {}}
						size='medium'
						color='primary'
					/>
				</li>
			</ul>
		</header>
	);
}
export default Header;
