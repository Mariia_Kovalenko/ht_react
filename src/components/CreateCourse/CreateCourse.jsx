import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import './CreateCourse.scss';
import { useState } from 'react';
import {
	AUTHOR_NAME_LABEL,
	CREATE_AUTHOR_BTN,
	ADD_AUTHOR_ACTION,
	DELETE_AUTHOR_ACTION,
	AUTHOR_INPUT_ERROR_MESSAGE,
	DESCRIPTION_INPUT_ERROR_MESSAGE,
	DURATION_INPUT_ERROR_MESSAGE,
	TITLE_INPUT_ERROR_MESSAGE,
} from '../../constants';
import { v4 as uuid } from 'uuid';
import { ValidateInputs } from '../../helpers/validation';
import CoursesAuthor from '../Courses/components/CoursesAuthor/CoursesAuthor';
import { generateCreationDate } from '../../helpers/dateGenerator';
import { pipeDuration } from '../../helpers/pipeDuration';

class Author {
	constructor(id, name) {
		this.id = id;
		this.name = name;
	}
}

class Course {
	constructor(id, title, description, creationDate, duration, authors) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.creationDate = creationDate;
		this.duration = duration;
		this.authors = authors;
	}
}

function CreateCourse({
	changePageState,
	courses,
	setCourses,
	authors,
	setAuthors,
}) {
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [values, setValues] = useState({
		title: '',
		description: '',
		authorName: '',
		creationDate: '',
		duration: '',
	});
	const [inputError, setInputError] = useState({
		title: false,
		description: false,
		author: false,
		duration: false,
	});

	const validateInputs = new ValidateInputs();

	const renderAuthorsList = (authors, option) => {
		let action = option + ' author';
		return authors.map((author) => {
			return (
				<CoursesAuthor
					key={author.id}
					name={author.name}
					action={action}
					onAction={handleAuthorAction(action, author.id)}
				/>
			);
		});
	};

	const handleChange = (prop) => (event) => {
		setValues({ ...values, [prop]: event.target.value });
	};

	const handleAuthorAction = (action, id) => (event) => {
		event.preventDefault();
		let authorFoundIndex;
		switch (action) {
			case 'Add author':
				authorFoundIndex = authors.indexOf((el) => el.id === id);
				if (authorFoundIndex) {
					const removedAuthor = authors.splice(authorFoundIndex, 1);
					setCourseAuthors([...courseAuthors, removedAuthor[0]]);
				}
				break;
			case 'Delete author':
				authorFoundIndex = courseAuthors.indexOf((el) => el.id === id);
				if (authorFoundIndex) {
					const removedAuthor = courseAuthors.splice(authorFoundIndex, 1);
					setAuthors([...authors, removedAuthor[0]]);
				}
				break;
			default:
				break;
		}
	};

	const addAuthor = (event) => {
		event.preventDefault();
		// validate name
		if (!validateInputs.validateName(values.authorName)) {
			setInputError({ ...inputError, author: true });
			return;
		}
		const _id = uuid();
		const newAuthor = new Author(_id, values.authorName);
		setAuthors([...authors, newAuthor]);
		setValues({ ...values, authorName: '' });
		setInputError({ ...inputError, autor: false });
	};

	const validateForm = () => {
		let titleError = false;
		let descriptionError = false;
		let durationError = false;

		if (!validateInputs.validateName(values.title)) {
			titleError = true;
		}
		if (!validateInputs.validateDescription(values.description)) {
			descriptionError = true;
		}

		if (!validateInputs.validateDuration(values.duration)) {
			durationError = true;
		}

		setInputError({
			...inputError,
			title: titleError,
			description: descriptionError,
			duration: durationError,
		});

		return !(titleError || descriptionError || durationError);
	};

	const createCourse = (event) => {
		event.preventDefault();

		if (!validateForm()) {
			return;
		}

		const _id = uuid();
		const creationDate = generateCreationDate();
		const newCourse = new Course(
			_id,
			values.title,
			values.description,
			creationDate,
			values.duration,
			courseAuthors.map((course) => course.id)
		);
		setCourses([...courses, { ...newCourse }]);
		setValues({
			title: '',
			description: '',
			authorName: '',
			creationDate: '',
			duration: '',
		});
		setAuthors([...authors, ...courseAuthors]);
		setCourseAuthors([]);

		if (inputError.author) {
			setInputError({
				title: false,
				description: false,
				author: false,
				duration: false,
			});
		}
	};

	return (
		<form className='form'>
			<div className='create-course'>
				<div className='create-course__inner'>
					<Input
						labelText='Title'
						value={values.title}
						onChange={handleChange('title')}
						error={inputError.title}
						errorMessage={TITLE_INPUT_ERROR_MESSAGE}
					/>
					<div className='create-course__buttons'>
						<Button
							buttonText={'Create course'}
							color='primary'
							size='medium'
							onClick={createCourse}
						/>
						<Button
							buttonText={'Back to courses'}
							color='grey'
							size='medium'
							onClick={changePageState}
						/>
					</div>
				</div>
				<Input
					textArea
					fullWidth={true}
					labelText='Description'
					value={values.description}
					onChange={handleChange('description')}
					error={inputError.description}
					errorMessage={DESCRIPTION_INPUT_ERROR_MESSAGE}
				/>
				<div className='form-course-details'>
					<div className='form-section'>
						<h4 className='form-section__title'>Author</h4>
						<Input
							labelText={AUTHOR_NAME_LABEL}
							placeholdetText='Enter author name'
							fullWidth
							error={inputError.author}
							errorMessage={AUTHOR_INPUT_ERROR_MESSAGE}
							value={values.authorName}
							onChange={handleChange('authorName')}
						/>
						<Button
							buttonText={CREATE_AUTHOR_BTN}
							size='medium'
							color='dark'
							onClick={addAuthor}
						/>
					</div>
					<div className='form-section'>
						<h4 className='form-section__title'>Authors</h4>
						{renderAuthorsList(authors, ADD_AUTHOR_ACTION)}
					</div>
					<div className='form-section'>
						<h4 className='form-section__title'>Duration</h4>
						<Input
							labelText={'Duration'}
							placeholdetText='Enter duration in minutes'
							fullWidth
							value={values.duration}
							onChange={handleChange('duration')}
							error={inputError.duration}
							errorMessage={DURATION_INPUT_ERROR_MESSAGE}
						/>
						<div className='duration'>
							Duration:{' '}
							<span style={{ fontWeight: 600 }}>
								{pipeDuration(values.duration)}
							</span>{' '}
							hours
						</div>
					</div>
					<div className='form-section'>
						<h4 className='form-section__title'>Course Authors</h4>
						{renderAuthorsList(courseAuthors, DELETE_AUTHOR_ACTION)}
					</div>
				</div>
			</div>
		</form>
	);
}

export default CreateCourse;
