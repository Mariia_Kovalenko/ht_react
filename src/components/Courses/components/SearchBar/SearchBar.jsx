import { useState } from 'react';
import Button from '../../../../common/Button/Button';
import Input from '../../../../common/Input/Input';
import './SearchBar.scss';

function SearchBar({ fetchCourses, changePageState }) {
	const [searchValue, setSearchValue] = useState('');

	const changeSearchValue = (e) => {
		if (!e.target.value) {
			// fetch all courses
			fetchCourses('');
		}
		setSearchValue(e.target.value);
	};

	const searchCourses = () => {
		fetchCourses(searchValue);
		setSearchValue('');
	};

	return (
		<div className='search-bar'>
			<div className='search-bar__panel'>
				<Input
					placeholdetText={'Enter course name or id...'}
					labelText={'Name or ID'}
					value={searchValue}
					onChange={changeSearchValue}
				/>
				<Button
					buttonText={'Search'}
					color='dark'
					size='medium'
					onClick={searchCourses}
				/>
			</div>
			<div className='search-bar__button'>
				<Button
					buttonText={'Add new Course'}
					color='primary'
					size='medium'
					onClick={changePageState}
				/>
			</div>
		</div>
	);
}

export default SearchBar;
