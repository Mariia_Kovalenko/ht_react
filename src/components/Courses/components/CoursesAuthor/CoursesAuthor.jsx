import Button from '../../../../common/Button/Button';
import './CoursesAuthor.scss';

function CoursesAuthor({ name, action, onAction }) {
	return (
		<div className='courses-author author'>
			<p className='author__name'>{name}</p>
			<Button
				buttonText={action}
				onClick={onAction}
				size='small'
				color='grey'
			/>
		</div>
	);
}

export default CoursesAuthor;
