import Button from '../../../../common/Button/Button';
import './CoursesCard.scss';
import { pipeDuration } from '../../../../helpers/pipeDuration';

function CoursesCard({ title, duration, creationDate, description, authors }) {
	return (
		<div className='courses__card card'>
			<div className='card__info'>
				<h4 className='card__title'>{title}</h4>
				<p className='card__desc'>{description}</p>
			</div>
			<div className='card__details details'>
				<div className='details__item detail'>
					<div className='detail__name'>Authors:</div>
					<div className='detail__info'>{authors}</div>
				</div>
				<div className='details__item detail'>
					<div className='detail__name'>Duration:</div>
					<div className='detail__info'>{pipeDuration(duration)} hours</div>
				</div>
				<div className='details__item detail'>
					<div className='detail__name'>Created:</div>
					<div className='detail__info'>{creationDate}</div>
				</div>
				<Button
					buttonText={'Show Course'}
					color='dark'
					size='medium'
					fullWidth={true}
				/>
			</div>
		</div>
	);
}

export default CoursesCard;
