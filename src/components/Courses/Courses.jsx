import { useState } from 'react';
import AlertMessage from '../../common/MessageBox/MessageBox';
import CoursesCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import './Courses.scss';

function Courses({ changePageState, courses, setCourses, authors }) {
	const maxAuthorsLength = 20;
	const allowedAuthorsLength = 23;
	const [error, setError] = useState(false);
	const [fetchedCourses, setFetchedCourses] = useState(courses);

	const trimAuthorsList = (authorsList) => {
		const result = authorsList.join(',');
		if (result.length > maxAuthorsLength) {
			return result.slice(0, allowedAuthorsLength) + '...';
		}
		return result;
	};

	const fetchCourses = (searchValue) => {
		if (error) {
			setError(false);
		}
		if (searchValue) {
			const coursesFound = courses.filter(
				(course) =>
					course.title.toUpperCase().includes(searchValue.toUpperCase()) ||
					course.id.toUpperCase().includes(searchValue.toUpperCase())
			);
			setFetchedCourses(coursesFound);
			if (!coursesFound.length) {
				// show message to user
				setError(true);
			}
		} else {
			setFetchedCourses(courses);
		}
	};

	return (
		<>
			<SearchBar
				fetchCourses={fetchCourses}
				changePageState={changePageState}
			/>
			<div className='courses'>
				{fetchedCourses.map((course) => {
					let authorsList = [];
					course.authors.forEach((authorId) => {
						const person = authors.find((person) => person.id === authorId);
						if (person) {
							authorsList.push(person.name);
						}
					});
					return (
						<CoursesCard
							key={course.id}
							title={course.title}
							duration={course.duration}
							creationDate={course.creationDate}
							description={course.description}
							authors={trimAuthorsList(authorsList)}
						/>
					);
				})}

				{error ? (
					<AlertMessage message={'No courses found'} type={'warning'} />
				) : null}
			</div>
		</>
	);
}

export default Courses;
