import './MessageBox.scss';

function AlertMessage({ message, type }) {
	let alertClass = 'alert';
	switch (type) {
		case 'error':
			alertClass += ' red';
			break;
		case 'warning':
			alertClass += ' orange';
			break;
		case 'success':
			alertClass += ' green';
			break;
		default:
			alertClass += ' orange';
	}
	return <div className={alertClass}>{message}</div>;
}

export default AlertMessage;
