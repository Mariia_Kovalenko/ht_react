import './Button.scss';

function Button({ buttonText, onClick, size, color, fullWidth }) {
	const buttonColor = color ? ` ${color}` : '';
	const buttonType = size ? ` ${size}` : '';
	const buttonWidth = fullWidth ? ' full-width' : '';
	const buttonClassName = 'btn' + buttonType + buttonColor + buttonWidth;
	return (
		<button className={buttonClassName} onClick={onClick}>
			{buttonText}
		</button>
	);
}

export default Button;
