import './Input.scss';

function Input({
	labelText,
	placeholdetText,
	onChange,
	value,
	textArea,
	fullWidth,
	error,
	errorMessage,
}) {
	const inputType = textArea ? ' textarea' : '';
	const inputWidth = fullWidth ? ' fullWidth' : '';
	const inputError = error ? ' errorInput' : '';

	let inputClassNames = 'input' + inputType + inputWidth + inputError;
	return (
		<div className='input-container'>
			<label htmlFor='input' className='label'>
				{labelText}
			</label>
			{textArea ? (
				<textarea
					className={inputClassNames}
					rows={8}
					placeholder={placeholdetText}
					onChange={onChange}
				/>
			) : (
				<input
					id='input'
					type='text'
					className={inputClassNames}
					value={value}
					placeholder={placeholdetText}
					onChange={onChange}
				/>
			)}
			{error && <div className='error'>{errorMessage}</div>}
		</div>
	);
}

export default Input;
