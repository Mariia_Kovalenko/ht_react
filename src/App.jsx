import './App.css';
import Courses from './components/Courses/Courses';
import Header from './components/Header/Header';
import { useState } from 'react';
import CreateCourse from './components/CreateCourse/CreateCourse';
import { mockedCoursesList, mockedAuthorsList } from './constants';

function App() {
	const [isCoursesListVisible, setIsCoursesListVisible] = useState(true);
	const [courses, setCourses] = useState(mockedCoursesList);
	const [authors, setAuthors] = useState(mockedAuthorsList);

	const changePageState = (event) => {
		event.preventDefault();
		console.log('change page state');
		setIsCoursesListVisible((prev) => !prev);
	};

	const content = isCoursesListVisible ? (
		<Courses
			changePageState={changePageState}
			courses={courses}
			setCourses={setCourses}
			authors={authors}
		/>
	) : (
		<CreateCourse
			changePageState={changePageState}
			courses={courses}
			setCourses={setCourses}
			authors={authors}
			setAuthors={setAuthors}
		/>
	);
	return (
		<div>
			<Header />
			<div className='container'>{content}</div>
		</div>
	);
}
export default App;
